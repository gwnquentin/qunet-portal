# Full Page Intro & Navigation
 _A Pen created at CodePen.io. Original URL: [https://codepen.io/GwnQuentin/pen/bGGGdoW](https://codepen.io/GwnQuentin/pen/bGGGdoW).

 Here is a responsive intro page, focused around a full width background image and a bold animated menu. And, for browsers that support it, a nice iOS-like blurred effect behind the navigation.

Article and download on Cody:
http://codyhouse.co/gem/intro-page-full-width-navigation/